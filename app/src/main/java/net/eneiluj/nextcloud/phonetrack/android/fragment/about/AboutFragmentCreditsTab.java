package net.eneiluj.nextcloud.phonetrack.android.fragment.about;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.eneiluj.nextcloud.phonetrack.R;
import net.eneiluj.nextcloud.phonetrack.util.SupportUtil;

public class AboutFragmentCreditsTab extends Fragment {

    TextView aboutVersion;
    TextView aboutMaintainer;
    TextView aboutTranslators;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about_credits_tab, container, false);
        aboutVersion = v.findViewById(R.id.about_version);
        aboutMaintainer = v.findViewById(R.id.about_maintainer);
        aboutTranslators = v.findViewById(R.id.about_translators);
        SupportUtil.setHtml(aboutVersion, R.string.about_version, "v" + SupportUtil.getAppVersionName(getActivity()));
        SupportUtil.setHtml(aboutMaintainer, R.string.about_maintainer);
        SupportUtil.setHtml(aboutTranslators, R.string.about_translators_crowdin, getString(R.string.url_translations));
        return v;
    }
}